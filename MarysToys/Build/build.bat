ECHO OFF

REM Set variable for output folder.
SET outputFolder=Release

REM Set variable for source folder.
SET sourceFolder=..\Source

REM Copy the files from the source folder to the output folder.
REM The /s switch tell it to copy all subfile and subfolders.
ECHO --- Copying files to output folder ---
XCOPY /s "%sourceFolder%" "%outputFolder%\"

ECHO --- Compressing/obfuscating JavaScript file ---
java -jar yuicompressor-2.4.8.jar %outputFolder%\js\site.js -o %outputFolder%\js\site.js

ECHO --- Compressing CSS file ---
java -jar yuicompressor-2.4.8.jar %outputFolder%\css\site.css -o %outputFolder%\css\site.css

ECHO Done!